import 'package:flutter/material.dart';

void main() => runApp(HelloFlutterApp());

class HelloFlutterApp extends StatefulWidget {
  @override
  _MyStatefulWidgetState createState() => _MyStatefulWidgetState();
}
String englishGreeting = "Hello Flutter";
String spanishGreeting = "Hola Flutter";
String frenchGreeting = "Bonjour Flutter";
String germanGreeting = "Hallo Greeting";
class _MyStatefulWidgetState extends State<HelloFlutterApp> {
  String displayText = englishGreeting;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: AppBar(
          title: Text("Hello Flutter"),
          leading: Icon(Icons.home),
          actions: <Widget>[
            IconButton(
                onPressed: () {
                  setState(() {
                    displayText = displayText == englishGreeting?
                    spanishGreeting : englishGreeting;
                  });

                },
                icon: Icon(Icons.refresh)),
            IconButton(
                onPressed: () {
                  setState(() {
                    displayText = displayText == englishGreeting ? frenchGreeting : englishGreeting;
                  });
                } ,
                icon: Icon(Icons.ac_unit_sharp)) ,

            IconButton(
                onPressed: () {
                  setState(() {
                    displayText = displayText == englishGreeting ? germanGreeting : englishGreeting;
                  });
                } ,
                icon: Icon(Icons.abc_sharp))
          ]
        ),
        body: Center (
          child: Text(
            displayText,
              style: TextStyle(fontSize: 24)
          ),
        ),
      ),
    );
  }
}



//SafeArea Widget
// class HelloFlutterApp extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//       debugShowCheckedModeBanner: false,
//       home: Scaffold(
//         appBar: AppBar(
//           title: Text("Hello Flutter"),
//           leading: Icon(Icons.home),
//           actions: <Widget>[
//             IconButton(
//                 onPressed: () {},
//                 icon: Icon(Icons.refresh))
//           ],
//         ),
//         body: Center (
//           child: Text(
//             "Hello Flutter",
//             style: TextStyle(fontSize: 24)
//           ),
//         ),
//       ),
//     );
//   }
// }
